﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicTacToe_Cell
{
    private int _row, _column;
    private SpriteRenderer _spr;
    private Sprite[] _xOSprites;
    private Vector2 _position;

    public int Row
    {
        get { return _row; }
    }

    public int Column
    {
        get { return _column; }
    }

    public float PosX
    {
        get { return _position.x; }
    }

    public float PosY
    {
        get { return _position.y; }
    }

    public float Bounds
    {
        get { return _spr.bounds.extents.x; }
    }

    public TicTacToe_Cell(int row, int column, Transform transform, Sprite[] xOSprites)
    {
        _row = row;
        _column = column;
        _spr = transform.GetComponent<SpriteRenderer>();
        _xOSprites = xOSprites;
        _position = transform.position;
    }

    public void ClearCells()
    {
        _spr.sprite = null;
    }

    public void Occupy(bool isX)
    {
        _spr.sprite = _xOSprites[isX ? 0 : 1];
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class TicTacToe_Board : MonoBehaviour
{
    [SerializeField]
    private Transform _gameBoard;

    [SerializeField]
    private Sprite[] _xOSprites;

    [SerializeField]
    private Transform _winLine;

    [SerializeField]
    private Text _infoText;


    private List<TicTacToe_Cell> _cellList;

    private Mouse _mouse;
    private float _inputBounds;
    private bool _isLocalPlayerX;

    private void OnEnable()
    {
        TicTacToe_GameManager.OnSetXorO += SetLocalPlayerXO;
        TicTacToe_GameManager.OnSetTurn += SetTurn;
        TicTacToe_GameManager.OnCellOccupiedResponse += SelectCell;
        TicTacToe_GameManager.OnGameOver += GameOver;
    }

    private void OnDisable()
    {
        TicTacToe_GameManager.OnSetXorO -= SetLocalPlayerXO;
        TicTacToe_GameManager.OnSetTurn -= SetTurn;
        TicTacToe_GameManager.OnCellOccupiedResponse -= SelectCell;
        TicTacToe_GameManager.OnGameOver -= GameOver;
    }

    private void Start()
    {
        PopulateCells();
        _mouse = InputSystem.GetDevice<Mouse>();
    }

    private void PopulateCells()
    {
        _cellList = new List<TicTacToe_Cell>();
        for (int i = 0; i < _gameBoard.childCount; i++)
        {
            int row = i / 3;
            int column = i % 3;
            _cellList.Add(new TicTacToe_Cell(row, column, _gameBoard.GetChild(i), _xOSprites));
        }

        _inputBounds = _cellList[0].Bounds;
        _cellList.ForEach(x => x.ClearCells());
    }

    private void Update()
    {
        if (_mouse.leftButton.wasReleasedThisFrame)
        {
            CheckPosition(Camera.main.ScreenToWorldPoint(_mouse.position.ReadValue()));
        }
    }

    private void SetLocalPlayerXO(bool isX)
    {
        _isLocalPlayerX = isX;
    }

    private void CheckPosition(Vector2 inputPos)
    {
        for (int i = 0; i < _cellList.Count; i++)
        {
            if (inputPos.x < _cellList[i].PosX + _inputBounds && inputPos.x > _cellList[i].PosX - _inputBounds
                                                              && inputPos.y < _cellList[i].PosY + _inputBounds && inputPos.y > _cellList[i].PosY - _inputBounds)
            {
                TicTacToe_GameManager.Instance.SelectCellRequest(_cellList[i].Row, _cellList[i].Column);
            }
        }
    }

    private void SetTurn(bool isMyTurn)
    {
        _infoText.text = isMyTurn ? "Your Turn" : "Opponent's Turn";
    }

    private void SelectCell(int row, int column, bool isLocalPlayer)
    {
        var cell = _cellList.Find(x => x.Row == row && x.Column == column);
        if (cell != null)
        {
            if (isLocalPlayer)
                cell.Occupy(_isLocalPlayerX);
            else
                cell.Occupy(!_isLocalPlayerX);
        }
    }

    private void GameOver(TicTacToe_EWinCondition winCondition, int row, int col, bool isLocalPlayerWin)
    {
        _infoText.text = "You " + (isLocalPlayerWin ? "Win!" : "Loose!");
        switch (winCondition)
        {
            case TicTacToe_EWinCondition.Horizontal:
                var cell = _cellList.Find(x => x.Row == row && x.Column == 1);
                _winLine.position = new Vector3(cell.PosX, cell.PosY);
                _winLine.eulerAngles = new Vector3(0, 0, 90);
                break;
            case TicTacToe_EWinCondition.Vertical:
                cell = _cellList.Find(x => x.Row == 1 && x.Column == col);
                _winLine.position = new Vector3(cell.PosX, cell.PosY);
                _winLine.eulerAngles = Vector3.zero;
                break;
            case TicTacToe_EWinCondition.DiagonalLeft:
                _winLine.position = Vector3.zero;
                _winLine.eulerAngles = new Vector3(0, 0, 45);
                break;
            case TicTacToe_EWinCondition.DiagonalRight:
                _winLine.position = Vector3.zero;
                _winLine.eulerAngles = new Vector3(0, 0, -45);
                break;
        }

        _winLine.gameObject.SetActive(true);
    }
}
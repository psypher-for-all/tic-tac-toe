﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Entities.Variables;
using Sfs2X.Requests;

public class TicTacToe_SFSGameNetwork : MonoBehaviour
{
    private byte _myPlayerID;
    private SmartFox _sfs;

    private void OnEnable()
    {
        TicTacToe_GameManager.OnSelectCellRequest += SendSelectCellRequest;
    }

    private void OnDisable()
    {
        TicTacToe_GameManager.OnSelectCellRequest -= SendSelectCellRequest;
    }

    void Start()
    {
        _sfs = SFSEventManager.Connection;
        _myPlayerID = (byte) _sfs.MySelf.PlayerId;
        _sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, ExtensionResponse);
        TicTacToe_GameManager.Instance.SetLocalPlayerXO(_myPlayerID == 1);
        SendPlayerReady();
    }

    void SendPlayerReady()
    {
        _sfs.Send(new ExtensionRequest("pR", new SFSObject(), _sfs.LastJoinedRoom));
    }

    void ExtensionResponse(BaseEvent evnt)
    {
        string cmd = (string) evnt.Params["cmd"];
        SFSObject dataObject = (SFSObject) evnt.Params["params"];
        switch (cmd)
        {
            case "cO": //cell occupied
                byte row = dataObject.GetByte("r");
                byte col = dataObject.GetByte("c");
                byte playerID = dataObject.GetByte("pID");
                TicTacToe_GameManager.Instance.SetCellOccupied(row, col, _myPlayerID == playerID);
                break;
            case "cT": //set turn
                playerID = dataObject.GetByte("pID");
                Debug.Log(playerID == _myPlayerID);
                TicTacToe_GameManager.Instance.SetCurrentTurn(_myPlayerID == playerID);
                break;
            case "gE": //game end
                playerID = dataObject.GetByte("wID");
                row = dataObject.GetByte("r");
                col = dataObject.GetByte("c");
                byte winCondition = dataObject.GetByte("wC");
                TicTacToe_GameManager.Instance.SetGameOver((TicTacToe_EWinCondition) winCondition, row, col, playerID == _myPlayerID);
                break;
        }
    }

    void SendSelectCellRequest(byte row, byte col)
    {
        SFSObject sfsObject = new SFSObject();
        sfsObject.PutByte("r", row);
        sfsObject.PutByte("c", col);
        _sfs.Send(new ExtensionRequest("sC", sfsObject, _sfs.LastJoinedRoom));
    }
}
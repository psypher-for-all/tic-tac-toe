﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicTacToe_GameData
{
}

public enum TicTacToe_EWinCondition
{
    None,
    Horizontal,
    Vertical,
    DiagonalLeft,
    DiagonalRight
}
﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Data;
using UnityEngine;

public class TicTacToe_GameManager : MonoBehaviour
{
    public static TicTacToe_GameManager Instance;

    public static Action<bool> OnSetXorO;
    public static Action<byte, byte> OnSelectCellRequest;
    public static Action<int, int, bool> OnCellOccupiedResponse;
    public static Action<bool> OnSetTurn;
    public static Action<TicTacToe_EWinCondition, int, int, bool> OnGameOver;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        DontDestroyOnLoad(this);
    }

    public void SetLocalPlayerXO(bool isX)
    {
        OnSetXorO?.Invoke(isX);
    }

    public void SetCurrentTurn(bool isLocalPlayer)
    {
        OnSetTurn?.Invoke(isLocalPlayer);
    }

    public void SelectCellRequest(int row, int col)
    {
        OnSelectCellRequest?.Invoke((byte) row, (byte) col);
    }

    public void SetCellOccupied(int row, int col, bool islocalPlayer)
    {
        OnCellOccupiedResponse?.Invoke(row, col, islocalPlayer);
    }

    public void SetGameOver(TicTacToe_EWinCondition winCondition, int row, int col, bool isLocalPlayerWinner)
    {
        OnGameOver?.Invoke(winCondition, row, col, isLocalPlayerWinner);
    }
}
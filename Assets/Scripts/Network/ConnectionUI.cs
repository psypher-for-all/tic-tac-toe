﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConnectionUI : MonoBehaviour
{
    [SerializeField]
    private InputField _zoneField, _hostIPField, _usernameField;

    [SerializeField]
    private Transform _logInPanel, _loggedInPanel;

    [SerializeField]
    private Text _userNameText;

    [SerializeField]
    private Button _joinRoomButtonPrefab;

    [SerializeField]
    private Text _errorMessage1Text, _errorMessage2Text;


    void Start()
    {
        SFSEventManager.SFSCallBacks.OnConnection += OnSFSConnection;
        SFSEventManager.SFSCallBacks.OnConnectionLost += OnSFSConnectionLost;
        SFSEventManager.SFSCallBacks.OnLogin += OnSFSLogin;
        SFSEventManager.SFSCallBacks.OnLoginError += OnSFSLoginFailed;
        SFSEventManager.SFSCallBacks.OnRoomsUpdated += OnSFSRoomsUpdated;
        SFSEventManager.SFSCallBacks.OnRoomJoin += OnSFSRoomJoined;
        SFSEventManager.SFSCallBacks.OnRoomJoinError += OnSFSRoomJoinError;
    }

    void OnDisable()
    {
    }

    void OnSFSConnection(bool isConnected)
    {
        if (isConnected)
            SFSEventManager.Instance.Login(_usernameField.text);
        else
            _errorMessage1Text.text = "Is the SmartFox server running?";
    }

    void OnSFSConnectionLost(string errorMessage)
    {
        _logInPanel.gameObject.SetActive(true);
        _loggedInPanel.gameObject.SetActive(false);
        _errorMessage1Text.text = errorMessage;
    }

    void OnSFSLogin(string username)
    {
        _logInPanel.gameObject.SetActive(false);
        _userNameText.text = "Welcome, " + username;
        _loggedInPanel.gameObject.SetActive(true);
    }

    void OnSFSLoginFailed(string message)
    {
        _errorMessage1Text.text = message;
    }

    void OnSFSRoomJoined(string roomName)
    {
        if (roomName.Equals("The Lobby")) return;

        _logInPanel.parent.gameObject.SetActive(false);
        _errorMessage2Text.text = "";
        SceneManager.LoadScene(1);
    }

    void OnSFSRoomJoinError(string errorMessage)
    {
        _errorMessage2Text.text = errorMessage;
    }

    void OnSFSRoomsUpdated(List<string> roomNameList)
    {
        for (int i = 1; i < _joinRoomButtonPrefab.transform.parent.childCount; i++)
        {
            Destroy(_joinRoomButtonPrefab.transform.parent.GetChild(1).gameObject);
        }

        foreach (string room in roomNameList)
        {
            if (!room.Equals("The Lobby"))
                CreateNewRoomButton(room);
        }
    }

    void CreateNewRoomButton(string roomName)
    {
        GameObject newRoomButton = Instantiate(_joinRoomButtonPrefab.gameObject, _joinRoomButtonPrefab.transform.parent);
        newRoomButton.name = roomName;
        newRoomButton.transform.GetChild(0).GetComponent<Text>().text = roomName;
        newRoomButton.GetComponent<Button>().onClick.AddListener(() => SFSEventManager.Instance.JoinRoom(roomName));
        newRoomButton.SetActive(true);
    }

    public void OnLoginClick()
    {
        if (_usernameField.text.Equals(""))
        {
            _errorMessage1Text.text = "Username cannot be left blank";
        }
        else
        {
            _errorMessage1Text.text = "";
            SFSEventManager.Instance.ConnectToServer(_hostIPField.text, _zoneField.text);
        }
    }

    public void OnStartGameClick()
    {
        SFSEventManager.Instance.CreateRoom(_usernameField.text + "'s Game");
    }
}
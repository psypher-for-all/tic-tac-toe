﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Variables;
using Sfs2X.Requests;

public class SFSEventManager : MonoBehaviour
{
    public static SFSEventManager Instance;
    public static SFSCallBacks SFSCallBacks;

    public string HostIp = "127.0.0.1";
    public int Port = 9933;
    public string Zone = "BasicExamples";
    public int PlayersPerGame = 2;

    static SmartFox _sfs;

    //for the server extension 
    private const string EXTENSION_ID = "TicTacToe";
    private const string EXTENSION_CLASS = "com.PI.TicTacToe.TicTacToe_Extension";

    public static SmartFox Connection
    {
        get { return _sfs; }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (_sfs != null)
            _sfs.ProcessEvents();
    }

    protected void OnApplicationQuit()
    {
        // Always disconnect before quitting
        if (_sfs != null && _sfs.IsConnected)
            _sfs.Disconnect();
    }

    void ResetAll()
    {
        _sfs.RemoveAllEventListeners();
    }

    public void ConnectToServer(string hostIP, string zone)
    {
        _sfs = new SmartFox();
        //connection events
        _sfs.AddEventListener(SFSEvent.CONNECTION, SFSConnection);
        _sfs.AddEventListener(SFSEvent.CONNECTION_LOST, SFSConnectionLost);

        //login events
        _sfs.AddEventListener(SFSEvent.LOGIN, SFSLogin);
        _sfs.AddEventListener(SFSEvent.LOGIN_ERROR, SFSLoginError);

        //room join events
        _sfs.AddEventListener(SFSEvent.ROOM_JOIN, SFSRoomJoin);
        _sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, SFSRoomJoinError);
        _sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, SFSUserEnterRoom);
        _sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, SFSUserExitRoom);

        //room create events
        _sfs.AddEventListener(SFSEvent.ROOM_ADD, SFSRoomAdded);
        _sfs.AddEventListener(SFSEvent.ROOM_REMOVE, SFSRoomRemoved);

        //setting config data for the connection
        ConfigData configData = new ConfigData();
        configData.Host = hostIP;
        configData.Port = Port;
        configData.Zone = zone;
        configData.Debug = true;

        _sfs.Connect(configData);
    }

    public void Login(string userName)
    {
        _sfs.Send(new Sfs2X.Requests.LoginRequest(userName));
    }

    public void JoinRoom(string roomName)
    {
        Room room = _sfs.RoomManager.GetRoomList().Find(x => x.Name == roomName);
        if (room.UserCount < PlayersPerGame)
            _sfs.Send(new JoinRoomRequest(roomName));
        else
            SFSCallBacks.OnRoomJoinError("Room does not have any more space.");
    }

    void SFSConnection(BaseEvent evnt)
    {
        if ((bool) evnt.Params["success"])
        {
            // Debug.Log("SFS Connection Established");
            SFSCallBacks.OnConnection?.Invoke(true);
        }
        else
        {
            // Debug.Log("Is the SFS Server Running");
            SFSCallBacks.OnConnection?.Invoke(false);
        }
    }

    void SFSConnectionLost(BaseEvent evnt)
    {
        // Debug.Log("SFS Connection Lost");
        ResetAll();
        SFSCallBacks.OnConnectionLost?.Invoke((string) evnt.Params["errorMessage"]);
    }

    void SFSLogin(BaseEvent evnt)
    {
        User user = (User) evnt.Params["user"];
        // Debug.Log("SFS Logged In as " + user.Name);
        SFSCallBacks.OnLogin?.Invoke(user.Name);
        _sfs.Send(new JoinRoomRequest("The Lobby"));
    }

    void SFSLoginError(BaseEvent evnt)
    {
        // Debug.Log("SFS Login Failed");
        SFSCallBacks.OnLoginError?.Invoke((string) evnt.Params["errorMessage"]);
    }

    void SFSRoomJoin(BaseEvent evnt)
    {
        Room room = (Room) evnt.Params["room"];
        // Debug.Log(room.Name + " Joined");
        SFSCallBacks.OnRoomJoin?.Invoke(room.Name);
        SFSCallBacks.OnRoomsUpdated?.Invoke(GetRoomNameList());
    }

    void SFSRoomJoinError(BaseEvent evnt)
    {
        Room room = (Room) evnt.Params["room"];
        // Debug.Log("Room " + room.Name + " join failed");
        SFSCallBacks.OnRoomJoinError?.Invoke((string) evnt.Params["errorMessage"]);
    }

    void SFSUserEnterRoom(BaseEvent evnt)
    {
        // Debug.Log("User Joined Room");
        User user = (User) evnt.Params["user"];
        SFSCallBacks.OnUserEnteredRoom?.Invoke(user.Name);
    }

    void SFSUserExitRoom(BaseEvent evnt)
    {
        // Debug.Log("User Left Room");
        User user = (User) evnt.Params["user"];
        SFSCallBacks.OnUserExitedRoom?.Invoke(user.Name);
    }

    void SFSRoomAdded(BaseEvent evnt)
    {
        Room room = (Room) evnt.Params["room"];
        // Debug.Log(room.Name + " Room Created");
        SFSCallBacks.OnRoomsUpdated?.Invoke(GetRoomNameList());
    }

    void SFSRoomRemoved(BaseEvent evnt)
    {
        Room room = (Room) evnt.Params["room"];
        // Debug.Log(room.Name + " Room Deleted");
        SFSCallBacks.OnRoomsUpdated?.Invoke(GetRoomNameList());
    }

    List<string> GetRoomNameList()
    {
        List<string> roomNameList = new List<string>();
        List<Room> roomList = _sfs.RoomManager.GetRoomList();
        roomNameList = roomList.Select(x => x.Name).ToList();
        return roomNameList;
    }

    public void CreateRoom(string roomName)
    {
        RoomSettings roomSettings = new RoomSettings(roomName);
        roomSettings.GroupId = "games";
        roomSettings.IsGame = true;
        roomSettings.MaxUsers = (short) PlayersPerGame;
        roomSettings.Extension = new RoomExtension(EXTENSION_ID, EXTENSION_CLASS);

        _sfs.Send(new CreateRoomRequest(roomSettings, true, _sfs.LastJoinedRoom));
    }

    void SFSUserVariableUpdate(BaseEvent evnt)
    {
        User user = (User) evnt.Params["user"];
        if (user == _sfs.MySelf) return;
        if (user.ContainsVariable("line"))
        {
            // GameManager.Instance.SetRemotePlayerMove(user.GetVariable("line").GetStringValue());
        }
    }

    public void SendLocalPlayerVariable(List<UserVariable> userVariables)
    {
        _sfs.Send(new SetUserVariablesRequest(userVariables));
    }
}

//class for callbacks

public struct SFSCallBacks
{
    //for connection
    public Action<bool> OnConnection;
    public Action<string> OnConnectionLost;

    //for Login
    public Action<string> OnLogin;
    public Action<string> OnLoginError;

    //for room join 
    public Action<string> OnRoomJoin;
    public Action<string> OnRoomJoinError;
    public Action<string> OnUserEnteredRoom;
    public Action<string> OnUserExitedRoom;

    //for room creation
    public Action<List<string>> OnRoomsUpdated;
}
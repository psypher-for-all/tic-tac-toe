package com.PI.TicTacToe;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class TicTacToe_GameBoard
{
    private int [][] _board;
    private int _rowCount,_colCount;

    public TicTacToe_GameBoard(int rowCount,int columnCount)
    {
        _board=new int[rowCount][columnCount];
        _rowCount=rowCount;
        _colCount=columnCount;
    }

    public boolean IsCellFree(int row,int column)
    {
        boolean isFree=false;
        if(_board[row][column]==0)
            isFree= true;
        return isFree;
    }

    public void SelectCell(int row, int column, int playerID)
    {
        _board[row][column]=playerID;
    }

    public TicTacToe_EWinConditions CheckWinCondition(int startRow,int startColumn)
    {
        List<Integer> rowList=new ArrayList<Integer>();
        List<Integer> columnList=new ArrayList<Integer>();

        for(int i=0;i<_rowCount;i++)
        {
            rowList.add(_board[startRow][i]);
            columnList.add(_board[i][startColumn]);
        }

        TicTacToe_EWinConditions winCondition=IsMatch(rowList)?TicTacToe_EWinConditions.Horizontal:IsMatch(columnList)?TicTacToe_EWinConditions.Vertical:TicTacToe_EWinConditions.None;
        if(winCondition==TicTacToe_EWinConditions.None)
            winCondition=GetDiagonalMatch();
        return winCondition;
    }

    private boolean IsMatch(List<Integer> cellList)
    {
        boolean isSame=true;
        for(int i=1;i<cellList.size();i++)
        {
            if(cellList.get(i)!=cellList.get(i-1) || cellList.get(i)==0)
            {
                isSame=false;
                break;
            }
        }
        return  isSame;
    }

    private TicTacToe_EWinConditions GetDiagonalMatch()
    {
        List<Integer> diagonalLeft=new ArrayList<Integer>();
        List<Integer> diagonalRight=new ArrayList<Integer>();

        for(int i=0;i<_rowCount;i++)
        {
            diagonalLeft.add(_board[i][i]);
            diagonalRight.add(_board[i][(_colCount-1)-i]);
        }

       TicTacToe_EWinConditions winCondition=IsMatch(diagonalLeft)?TicTacToe_EWinConditions.DiagonalLeft:IsMatch(diagonalRight)?TicTacToe_EWinConditions.DiagonalRight:TicTacToe_EWinConditions.None;
       return winCondition;
    }
}

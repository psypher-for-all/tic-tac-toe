package com.PI.TicTacToe;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class TicTacToe_SelectCellRequest extends BaseClientRequestHandler
{

    @Override
    public void handleClientRequest(User user, ISFSObject isfsObject)
    {
        TicTacToe_Extension extension =(TicTacToe_Extension) getParentExtension();
        extension.SelectCellRequest(isfsObject.getByte("r"), isfsObject.getByte("c"), (byte)user.getPlayerId());
    }
}

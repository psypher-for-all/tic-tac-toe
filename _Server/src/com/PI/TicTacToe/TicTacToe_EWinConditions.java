package com.PI.TicTacToe;

public enum TicTacToe_EWinConditions
{
    None,
    Horizontal,
    Vertical,
    DiagonalLeft,
    DiagonalRight
}

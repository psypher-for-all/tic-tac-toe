package com.PI.TicTacToe;

import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.SFSExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TicTacToe_Extension extends SFSExtension
{
    private boolean _isGameStarted;
    private List<Byte> _playersInGame;
    private TicTacToe_GameBoard _gameBoard;
    private byte _currentPlayerTurn;

    private Timer _timer;

    @Override
    public void init()
    {
        _playersInGame=new ArrayList<Byte>();
        addRequestHandler("pR", TicTacToe_PlayerReadyHandler.class);
        addRequestHandler("sC", TicTacToe_SelectCellRequest.class);
        trace("Ext started");
    }

    public void PlayerReady(byte playerID)
    {
        if(_playersInGame.contains(playerID))return;
        _playersInGame.add(playerID);
        trace("Player Ready");
        if(_playersInGame.size()==2)
        {
            if(_isGameStarted)return;
            _isGameStarted=true;
            StartGame();
            trace("Player Ready");
            SendCurrentTurn();
        }
    }

    private void StartGame()
    {
        _gameBoard=new TicTacToe_GameBoard(3,3);
        _currentPlayerTurn=0;
    }

    public void SelectCellRequest(byte row,byte col,byte playerID)
    {
        if(playerID!=_currentPlayerTurn)return;
        if(_gameBoard.IsCellFree(row, col))
        {
            _gameBoard.SelectCell(row, col, playerID);
            SendCellOccupied(row, col, playerID);
            _timer=new Timer();
            _timer.schedule(new OnCheckWinCondition(row,col), 500);
        }
    }

    private class OnCheckWinCondition extends TimerTask
    {
        private byte _row,_col;
        public OnCheckWinCondition(byte row, byte column)
        {
            _row=row;
            _col=column;
        }

        @Override
        public void run()
        {
            TicTacToe_EWinConditions winCondition=_gameBoard.CheckWinCondition(_row,_col);
            if(winCondition!=TicTacToe_EWinConditions.None)
                SendWinner(winCondition, _currentPlayerTurn,_row,_col);
            else
                SendCurrentTurn();
        }
    }

    private void SendCellOccupied(byte row,byte col,byte playerID)
    {
        SFSObject sfsObject=new SFSObject();
        sfsObject.putByte("r", row);
        sfsObject.putByte("c", col);
        sfsObject.putByte("pID", playerID);
        send("cO", sfsObject, getParentRoom().getUserList());
    }

    private void SendWinner(TicTacToe_EWinConditions eWinCondition,byte winnerID,byte row,byte col)
    {
        SFSObject sfsObject=new SFSObject();
        sfsObject.putByte("wID", winnerID);
        sfsObject.putByte("r", row);
        sfsObject.putByte("c", col);
        byte winCondition=(byte)eWinCondition.ordinal();
        sfsObject.putByte("wC", winCondition);
        send("gE", sfsObject, getParentRoom().getUserList());
    }

    private void SendCurrentTurn()
    {
        _currentPlayerTurn=(byte)(_currentPlayerTurn==1?2:1);
        SFSObject sfsObject=new SFSObject();
        sfsObject.putByte("pID", _currentPlayerTurn);
        send("cT", sfsObject, getParentRoom().getUserList());
    }
}
